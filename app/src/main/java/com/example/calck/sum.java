package com.example.calck;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class sum extends AppCompatActivity {
    TextView textSum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sum);
        textSum = (TextView)findViewById(R.id.textSum);
         Intent intent = getIntent();
        String resultSum = intent.getStringExtra("result");

        textSum.setText(resultSum);
    }
}
