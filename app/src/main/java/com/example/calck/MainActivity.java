package com.example.calck;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String firstText;
    String secondText;
    String result;

    Button button;
    EditText first,
             second;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.button);
        first = (EditText)findViewById(R.id.first);
        second = (EditText)findViewById(R.id.second);

        btnClick();
    }

    void btnClick(){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstText = first.getText().toString();
                secondText = second.getText().toString();

                result = Integer.toString(Integer.valueOf(firstText) + Integer.valueOf(secondText));

                Intent intent = new Intent(MainActivity.this, sum.class);
                intent.putExtra("result",result);
                startActivity(intent);
            }
        });
    }
}
